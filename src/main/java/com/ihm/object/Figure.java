package com.ihm.object;

import java.awt.Color;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import com.ihm.visuel.FenetrePrincipale;
import lombok.Data;

/**
 * Classe figure permet la création de celle-ci.
 * Elle permet de lister les points contenus dans la figure et
 * de modifier les paramètres de celle-ci.
 * 
 * listPoint est une ArrayList contenant les points.
 * nom est un String contenantle nom de la figure.
 * epaisseur est un int contenant l'epaisseur du trait.
 * typeTrait est un String contenant le type de trait.
 * couleur est un Color contenant la couleur des traits
 *
 * @author Brandon et Thierry
 */

@XmlRootElement(name = "figure")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class Figure {

	// Declaration des attributs
	private float epaisseur;
	private boolean typeTrait;
	private String couleur;
	public ArrayList<Point> listPoint;

	/**
	 * Contructeur par défaut.
	 */

	public Figure() {
		listPoint = new ArrayList<>();
		epaisseur = 1.0f;
		typeTrait = false;
		couleur = "";
	}

	/**
	 * Méthode ajoutPoint permet d'ajouter à la liste un nouveau point.
	 * 
	 * @param p contient la position du point.
	 * @param n contient le nom du point.
	 * @param a contient l'abscisse du point.
	 * @param b contient l'ordonnee du point.
	 * 
	 */

	public void ajoutPoint(int p, String n, int a, int b) {

		Point point = new Point(n, a, b);

		// Condition si la position est supérieur à la taille de la liste
		if (listPoint.size() < p) {
			listPoint.add(point);
		} else {
			listPoint.add(p - 1, point);
		}

		// Rafraichissement de l'interface visuel
		FenetrePrincipale.panneauDessin.repaint();
		FenetrePrincipale.panneauDessin.revalidate();
	}

	/**
	 * Méthode suppressionPoint permet de supprimer un point.
	 * 
	 * @param p index du point a supprimer.
	 */
	
	public void suppressionPoint(int p) {
		listPoint.remove(p);
	}

	/**
	 * Méthode retourColor qui permet de récupérer la couleur.
	 * 
	 * @param colorStr String contenant la couleur sous forme
	 * héxadécimal ou rgb.
	 * 
	 * @return Color.
	 */

	public Color retourColor(String colorStr) {

		// Condition si la couleur est au format hexadecimal
		if (colorStr.contains("@")) {
			return new Color(Integer.valueOf(colorStr.substring(1, 3), 16),
					Integer.valueOf(colorStr.substring(3, 5), 16), Integer.valueOf(colorStr.substring(5, 7), 16));
		}

		// Condition si la couleur est au format rgb
		else if (colorStr.contains("r=")) {
			
			// Suppression des crochet, et des indications de couleurs
			colorStr = colorStr.substring(colorStr.indexOf("[") + 1, colorStr.indexOf("]")).replace("r=", "")
					.replace("g=", "").replace("b=", "");
			
			// Mise en place des valeurs numériques dans un tableau
			String[] str = colorStr.split(",");
			return new Color(Integer.parseInt(str[0]), Integer.parseInt(str[1]), Integer.parseInt(str[2]));
		}
		return null;
	}
}