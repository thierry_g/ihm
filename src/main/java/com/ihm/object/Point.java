package com.ihm.object;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;

@XmlRootElement(name = "point")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class Point {

	// Déclaration des attributs
	private String nom;
	private int abscisse;
	private int ordonnee;

	/**
	 * Constructeur par défaut.
	 */
	public Point() {
	}

	/**
	 * Constructeur avec trois paramètres.
	 * 
	 * @param nom qui une un chaine de caractère contenant le nom.
	 * @param a qui est l'abscisse du point.
	 * @param b qui est l'ordonnee du point.
	 */
	
	public Point(String nom, int a, int b) {
		this.nom = nom.toUpperCase();
		this.abscisse = a;
		this.ordonnee = b;
	}

	/**
	 * Methode deplacer permet de changer l'abscisse et
	 * l'ordonnée d'un point.
	 * 
	 * @param dx nouvelle coordonner de l'abscisse.
	 * @param dy nouvelle coordonner de ordonne.
	 */
	
	public void deplacer(int dx, int dy) {
		this.abscisse += dx;
		this.ordonnee += dy;
	}
}