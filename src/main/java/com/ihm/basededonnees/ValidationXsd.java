package com.ihm.basededonnees;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.xml.sax.SAXException;
import com.ihm.config.Constantes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * La classe validation permet la vérification du format XML.
 * Elle permet de vérifier le format de la sauvegarde et du fichier à charger.
 * 
 * builder est un DocumentBuilder.
 * factory est un SchemaFactory.
 * validator est un Validator.
 * logger est le Logger pour la récupération d'erreur.
 * 
 * @author Thierry
 */

public class ValidationXsd {

	// Déclaration des attributs
	private static DocumentBuilder builder;
	private static SchemaFactory factory;
	private static Validator validator;
	private static final Logger logger = LoggerFactory.getLogger(ValidationXsd.class);

	/**
	 * Methode validation permet la vérification d'un fichier
	 * XML grâce à un fichier xsd.
	 * 
	 * @param xsd contient le schema XML.
	 * @param xml contient le fichier à vérifier.
	 * @return un boolean de validation.
	 */
	
	public static boolean validation(File xsd, File xml) {

		try {
			// Création du builder et du factory
			builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			factory = SchemaFactory.newInstance(javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI);
			
			// Création du validator avec le schema et lancement de la validation.
			validator = factory.newSchema(new StreamSource(ValidationXsd.class.getResourceAsStream("/projet.xsd")))
					.newValidator();
			validator.validate(new DOMSource(builder.parse(xml)));
			return true;

		} catch (SAXException | IOException | ParserConfigurationException e ) {
			logger.error(Constantes.LOGGER_WRONG_FORMAT_XML);
		}
		return false;
	}
}