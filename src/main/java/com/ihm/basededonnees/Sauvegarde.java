package com.ihm.basededonnees;

import java.io.File;
import javax.swing.JOptionPane;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.ihm.config.Constantes;
import com.ihm.object.Projet;

/**
 * La classe sauvegarde écrit les données sur un fichier XML.
 * Elle permet la sauvegarde d'un projet actuel sous format XML.
 * 
 * jaxb est un JAXBContext
 * unmarsh est un Marshaller
 * xsd est le fichier de validation
 * logger est le Logger pour la récupération d'erreur
 * 
 * @author Thierry
 */

public class Sauvegarde {
	
	// Déclaration des variables
	private static JAXBContext jaxbcontext;
	private static Marshaller marshaller;
	private static File xsd;
	private static final Logger logger = LoggerFactory.getLogger(Sauvegarde.class);

	/**
	 * Méthode ecritureFichier permet de sauvegarder le projet en cours
	 * dans un fichier choisi par l'utilisateur.
	 * 
	 * @param projet contient le projet à sauvegarder.
	 * @param file contient le fichier de sauvegarde.
	 */
	
	public static void ecritureFichier(Projet projet, File file) {
	
		try {

			// Création d'un objet de type JAXBContext et d'un objet Marshaller
			jaxbcontext = JAXBContext.newInstance(Projet.class);
			marshaller = jaxbcontext.createMarshaller();

			// Ecriture des données contenues dans le projet vers le fichier XML
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.marshal(projet, file);
			
			// Création du fichier de validation
			xsd = new File(Chargement.class.getResource(Constantes.PATH_XSD).getPath());

			// Condition pour l'affichage d'un message d'erreur en cas de mauvaise
			// sauvegarde
			if (!ValidationXsd.validation(xsd, file)) {
				JOptionPane.showMessageDialog(null, Constantes.LOGGER_WRONG_SAVE, "Erreur de sauvegarde",
						JOptionPane.ERROR_MESSAGE);
				logger.warn(Constantes.LOGGER_WRONG_FORMAT_XML);
			}

		} catch (JAXBException e) {
			logger.error(Constantes.LOGGER_WRONG_SAVE);
		}
	}
}