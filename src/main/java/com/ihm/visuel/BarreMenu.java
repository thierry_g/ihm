package com.ihm.visuel;

import static com.ihm.visuel.PanneauPoint.panneauListeDePoint;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.ihm.basededonnees.Chargement;
import com.ihm.basededonnees.Sauvegarde;
import com.ihm.config.Constantes;
import com.ihm.object.Fichier;

/**
 * La classe BarreMenu ajoute et gère le menu de l'application.
 * Elle permet d'accéder au différents items du menu.
 * 
 * mapMenu est une map contenant les item du menu.
 * menuFichier est le menu fichier.
 * menuHelp est le menu Help.
 * sauve est le fichier de sauvegarde.
 * menuBar est la barre du menu.
 * logger est le Logger pour la recuparation d'erreur.
 * 
 * @author Najim et Thierry
 */

public class BarreMenu {
	
	// Déclaration des attributs
	private static Map<String, JMenuItem> mapMenu = new HashMap<>();
	private JMenu menuFichier = new JMenu("Fichier");
	private JMenu menuHelp = new JMenu("Help");
	private static File sauve = null;
	JMenuBar menuBar = new JMenuBar();
	private static final Logger logger = LoggerFactory.getLogger(BarreMenu.class);

	/**
	 * Constructeur par défaut.
	 */
	
	public BarreMenu() {

		this.menuBar.add(menuFichier);
		this.menuBar.add(menuHelp);
		BarreMenu.ajoutBarreMenu(menuFichier, "Nouveau", "Ouvrir", "Enregistrer", "Enregistrer sous", "Quitter");
		BarreMenu.ajoutBarreMenu(menuHelp, "A propos");
		BarreMenu.ajoutListnerItemMenu();
	}

	/**
	 * Méthode ajoutBarreMenu permet l'ajout de menu et d'item.
	 * 
	 * @param menu contient le menu à ajouter.
	 * @param liste contient les items du menus.
	 */
	
	public static void ajoutBarreMenu(JMenu menu, String... liste) {
		
		// Boucle pour ajouter chaque item au menu
		for (String str : liste) {
			JMenuItem jmu = new JMenuItem(str);
			menu.add(jmu);
			mapMenu.put(str, jmu);
		}
	}

	/**
	 * Méthode ajoutListnerItemMunu permet la gestion des écouteurs
	 * des différents items de la barre de menu.
	 */
	
	public static void ajoutListnerItemMenu() {
		
		/**
		 * Gestion de l'item nouveau dans le menu fichier
		 */
		
		mapMenu.get("Nouveau").addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				// Condition pour la sauvegarde du projet si celui-ci n'est 
				// pas sauvegarde
				if (sauve == null) {
					if (JOptionPane.showConfirmDialog(null,
							"Voulez vous sauvegarder votre projet avant d'en ouvrir un nouveau",
							"Votre projet n'est pas sauvegarde", JOptionPane.YES_NO_CANCEL_OPTION,
							JOptionPane.QUESTION_MESSAGE) == 0)
						sauvegardeFichier();
				}
				
				// Création du nouveau projet et remise à zéro du fichier sauve
				Fichier.newProject();
				sauve = null;
			}
		});
		
		/**
		 * Gestion de l'item ouvrir dans le menu fichier
		 */

		mapMenu.get("Ouvrir").addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {

				// Création de la fenêtre de choix de fichier
				JFileChooser chooser = new JFileChooser();
				FileNameExtensionFilter filter = new FileNameExtensionFilter("Fichier XML seulement", "xml");
				chooser.setFileFilter(filter);
				chooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
				int returnVal = chooser.showOpenDialog(null);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = chooser.getSelectedFile();
					sauve = file;

					// Condition si le fichier a la bonne extension
					if (file.getPath().endsWith(".xml")) {

						// try / catch gérant le null pointer exception en cas de problème de chargement
						try {
							Fichier.setProjet(Chargement.lectureFichier(file));
							Fichier.setFigureEnCours(Fichier.getProjet().getListFigure().get(0));
						} catch (NullPointerException npe) {
							JOptionPane.showMessageDialog(null, Constantes.LOGGER_WRONG_FILE_EMPTY,
									"Fichier vide", JOptionPane.ERROR_MESSAGE);
							logger.error(Constantes.LOGGER_WRONG_FILE_EMPTY);
							
						}
						PanneauPoint.rafraichirPoints();
						validation();
						logger.info(Constantes.LOGGER_SUCCESS_LOAD);
					}

					// Condition si le fichier n'a pas la bonne extension
					else {
						JOptionPane.showMessageDialog(null, "Vous ne pouvez charger que des fichiers \".xml\"",
								"Erreur de fichier", JOptionPane.ERROR_MESSAGE);
						logger.warn(Constantes.LOGGER_WRONG_FORMAT_FILE);
					}
				}
			}
		});

		/**
		 * Gestion de l'item enregistrer dans le menu fichier
		 */
		
		mapMenu.get("Enregistrer").addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				// Condition si aucun fichier de sauvegarde n'a ete defini
				if (sauve == null) {
					logger.warn(Constantes.LOGGER_WRONG_FILE_SELECT);
					sauvegardeFichier();
				}

				// Condition si un fichier de sauvegarde est defini
				else {
					Sauvegarde.ecritureFichier(Fichier.getProjet(), sauve);
					logger.info(Constantes.LOGGER_SUCCESS_SAVE);
				}
			}
		});

		/**
		 * Gestion de l'item enregistrer sous dans le menu fichier
		 */

		mapMenu.get("Enregistrer sous").addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				sauvegardeFichier();
			}
		});

		/**
		 * Gestion de l'item quitter dans le menu fichier
		 */

		mapMenu.get("Quitter").addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				logger.info(Constantes.LOGGER_EXIT);
				System.exit(1);
			}
		});
		
		/**
		 * Gestion de l'item a propos dans le menu help
		 */

		mapMenu.get("A propos").addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, Constantes.PROPOS);
			}
		});
	}

	/**
	 * MéthodesauvegardeFichier qui permet la sauvegarde du projet dans
	 * un fichier sélectionné par l'utilisateur.
	 */
	
	public static void sauvegardeFichier() {

		JFileChooser chooser = new JFileChooser();
		chooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
		int retrival = chooser.showSaveDialog(null);
		if (retrival == JFileChooser.APPROVE_OPTION) {
			try {
				sauve = new File(chooser.getSelectedFile() + ".xml");
				Sauvegarde.ecritureFichier(Fichier.getProjet(), sauve);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		logger.info(Constantes.LOGGER_SUCCESS_SAVE);
	}
	
	/**
	 * Méthode validation qui permet le rafraichissement de l'interface visuel
	 */

	public static void validation() {

		FenetrePrincipale.panneauDessin.repaint();
		FenetrePrincipale.panneauDessin.revalidate();
		panneauListeDePoint.repaint();
		panneauListeDePoint.validate();
		panneauListeDePoint.revalidate();
		FenetrePrincipale.panneauPoint.repaint();
		FenetrePrincipale.panneauPoint.revalidate();
	}
}