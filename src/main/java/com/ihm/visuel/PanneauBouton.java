package com.ihm.visuel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.ihm.config.Constantes;
import com.ihm.object.Fichier;

/**
 * comporte une liste de panneauPointLigne qui eux meme comportent, la position,
 * le nom, l'abscisse et l'ordonnée du point.
 * 
 * @author Najim
 */
public class PanneauBouton extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// menu déroulant: gère le type de trait, continu ou pointille
	private JComboBox<String> comboTypeTrait = new JComboBox<>();
	// menu déroulant: gère l'epaisseur
	private JComboBox<Integer> comboEpaisseurTrait = new JComboBox<>();
	// menu déroulant: gère le thème jour ou nuit
	private JComboBox<String> theme = new JComboBox<>();
	// bouton: bascule à la figure précédente
	private JButton precedent = new JButton(Constantes.PRECEDENT);
	// bouton: bascule à la figure suivante
	private JButton suivant = new JButton(Constantes.SUIVANT);
	// bouton: crée une nouvelle figure
	private JButton nouveau = new JButton(Constantes.NOUVEAU);

	private final Integer[] LISTEEPAISSEUR = new Integer[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	private JButton boutonChoixCouleur = new JButton();
	private static final Logger logger = LoggerFactory.getLogger(PanneauBouton.class);

	/**
	 * Contructeur: ajoutes tous les éléments au panel principal
	 */
	public PanneauBouton() {
		// config des différentes fonctionnalitées
		configMenuTheme();
		configMenuTypeTrait();
		configMenuEpaisseurTrait();
		configBoutonCouleur();
		configBoutonPrecedent();
		configBoutonSuivant();
		configBoutonNouveau();
	}

	/**
	 * Méthode configBoutonNouveau permet la création d'un nouvelle figure
	 */

	private void configBoutonNouveau() {
		nouveau.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!Fichier.getProjet().getListFigure().get(Fichier.getProjet().getListFigure().size() - 1)
						.getListPoint().isEmpty()) {
					Fichier.getProjet().ajoutFigure();
					logger.debug(Constantes.LOGGER_CREATE_FIGURE);
				} else {
					JOptionPane.showMessageDialog(null, "La derniere figure ne comporte pas de point",
							"Impossible d'ajouter une figure", JOptionPane.ERROR_MESSAGE);
					logger.warn(Constantes.LOGGER_FIGURE_EMPTY);
				}
			}
		});
		this.add(nouveau);
	}

	/**
	 * Méthode configMenuEpaisseurTrait permet de choisir l'épaisseur
	 */

	private void configMenuEpaisseurTrait() {
		for (int i : LISTEEPAISSEUR) {
			comboEpaisseurTrait.addItem(i);
		}
		comboEpaisseurTrait.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				int ep = (int) e.getItem();
				Fichier.getFigureEnCours().setEpaisseur(Float.valueOf(ep + ""));
				logger.debug(Constantes.LOGGER_UPDATE_SIZE);
				validationFenetre();
			}
		});
		this.add(new JLabel("Epaisseur"));
		this.add(comboEpaisseurTrait);

	}

	/**
	 * Méthode permet de choisir le type de trait
	 */

	private void configMenuTypeTrait() {
		comboTypeTrait.addItem(Constantes.TRAITCONTINU);
		comboTypeTrait.addItem(Constantes.TRAITPOINTILLE);
		comboTypeTrait.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (((String) e.getItem()).equals(Constantes.TRAITPOINTILLE))
					Fichier.getFigureEnCours().setTypeTrait(true);
				else
					Fichier.getFigureEnCours().setTypeTrait(false);

				logger.debug(Constantes.LOGGER_UPDATE_STYLE);
				validationFenetre();
			}
		});
		this.add(new JLabel("Type de trait"));
		this.add(comboTypeTrait);

	}

	/**
	 * Méthode permet de basculer entre les figures
	 */
	private void configBoutonSuivant() {
		/// Ecouteur du bouton suivant
		suivant.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Fichier.choixFigure(Constantes.SUIVANT);
				PanneauPoint.rafraichirPoints();
				logger.debug(Constantes.LOGGER_FIGURE_NEXT);
				validationFenetre();
			}
		});
		this.add(suivant);
	}

	/**
	 * Méthode permet de basculer entre les figures
	 */
	private void configBoutonPrecedent() {
		// Ecouteur du bouton precedent
		precedent.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Fichier.choixFigure(Constantes.PRECEDENT);
				PanneauPoint.rafraichirPoints();
				logger.debug(Constantes.LOGGER_FIGURE_PREVIOUS);
				validationFenetre();
			}
		});
		this.add(precedent);
	}

	/**
	 * Méthode permet de basculer entre les themes
	 */
	private void configMenuTheme() {
		theme.addItem(Constantes.THEME_JOUR);
		theme.addItem(Constantes.THEME_NUIT);
		theme.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getItem().equals(Constantes.THEME_JOUR)) {
					PanneauDessin.setDarkMode(false);
				} else {
					PanneauDessin.setDarkMode(true);
				}
				FenetrePrincipale.panneauDessin.repaint();
				logger.debug(Constantes.LOGGER_WINDOWS_STYLE);
				
			}
		});
		this.add(new JLabel("Theme"));
		this.add(theme);
	}

	/**
	 * Méthode permet de selectionner une couleur
	 */
	private void configBoutonCouleur() {
		boutonChoixCouleur.setBackground(Color.BLACK);
		boutonChoixCouleur.setPreferredSize(new Dimension(45, 25));
		boutonChoixCouleur.addMouseListener(new MouseListener() {
			@Override
			public void mouseReleased(MouseEvent e) {
			}

			@Override
			public void mousePressed(MouseEvent e) {
				try {
					Fichier.getFigureEnCours()
							.setCouleur(JColorChooser.showDialog(null, "couleur", Color.blue).toString());
					validationFenetre();
					boutonChoixCouleur.setBackground(
							Fichier.getFigureEnCours().retourColor(Fichier.getFigureEnCours().getCouleur()));
					logger.debug(Constantes.LOGGER_UPDATE_COLOR);
				} catch (NullPointerException ex) {
					logger.warn(Constantes.LOGGER_WRONG_COLOR);
				}
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseClicked(MouseEvent e) {
			}
		});
		this.add(new JLabel("Couleur"));
		this.add(boutonChoixCouleur);
	}

	public static void validationFenetre() {
		FenetrePrincipale.panneauDessin.repaint();
		FenetrePrincipale.panneauDessin.validate();
		;

	}

}