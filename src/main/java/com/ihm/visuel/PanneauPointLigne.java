package com.ihm.visuel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import com.ihm.config.Constantes;
import com.ihm.object.Fichier;
import com.ihm.object.Point;

/**
 * Classe permettant de générer des interfaces visuelles de lignes de points
 */

public class PanneauPointLigne extends JPanel {
	private static final long serialVersionUID = 1L;
	private JLabel posLab = new JLabel();
	private JTextField champsNom = new JTextField();
	private JTextField champsX = new JTextField();
	private JTextField champsY = new JTextField();
	private JButton boutonSupprPoint = new JButton(
			new ImageIcon(PanneauPointLigne.class.getResource(Constantes.PATH_POUBELLE)));


	/**
	 * @param position position du point.
	 * @param nom nom du point.
	 * @param X abscisse du point.
	 * @param Y ordonnee du point.
	 */
	public PanneauPointLigne(int position, String nom, int X, int Y) {
		configPosition(position);
		configNom(position, nom);
		configX(position, X);
		configY(position, Y);
		configBoutonSupp();
	}


	/**
	 * rend le bouton poubelle transparent et l'ajoute au panel
	 */
	private void configBoutonSupp() {
		boutonSupprPoint.setBorderPainted(false);
		boutonSupprPoint.setContentAreaFilled(false);
		boutonSupprPoint.setFocusPainted(false);
		boutonSupprPoint.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JButton b = (JButton) e.getSource();
				PanneauPointLigne p = (PanneauPointLigne) b.getParent();
				Fichier.getFigureEnCours().suppressionPoint(Integer.parseInt(p.posLab.getText()) - 1);

				PanneauPoint.panneauListeDePoint.removeAll();
				List<Point> list = Fichier.getFigureEnCours().listPoint;

				for (int i = 0; i < list.size(); i++) {
					PanneauPoint.panneauListeDePoint.add(new PanneauPointLigne(i,
							list.get(i).getNom(), list.get(i).getAbscisse(), list.get(i).getOrdonnee()));
				}
				refresh();
			}
		});
		this.add(boutonSupprPoint);
	}


	/**
	 * @param position position du point.
	 * @param Y ordonnee du point.
	 */
	private void configY(int position, int Y) {
		JPanel coupleChampsLabY = new JPanel();
		champsY = new JTextField(Y + "");
		champsY.setColumns(3);
		champsY.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void keyReleased(KeyEvent e) {
				if(champsY.getText().length()>0) {

					Fichier.getFigureEnCours().listPoint.get(position)
					.setOrdonnee(Integer.parseInt(champsY.getText()));
				}
				refresh();
			}

			@Override
			public void keyPressed(KeyEvent e) {
			}
		});
		coupleChampsLabY.add(new JLabel("Y"));

		coupleChampsLabY.add(champsY);
		this.add(coupleChampsLabY);


	}

	/**
	 * 
	 * @param position position du point.
	 * @param X abscisse du point.
	 */
	private void configX(int position, int X) {
		JPanel coupleChampsLabX = new JPanel();
		champsX = new JTextField(X + "");
		champsX.setColumns(3);
		champsX.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyReleased(KeyEvent e) {
				if (champsX.getText().length() > 0) {

					Fichier.getFigureEnCours().listPoint.get(position).setAbscisse(Integer.parseInt(champsX.getText()));
				}
				FenetrePrincipale.panneauDessin.repaint();
				FenetrePrincipale.panneauDessin.revalidate();
			}

			@Override
			public void keyPressed(KeyEvent e) {
			}
		});
		coupleChampsLabX.add(new JLabel("X"));
		coupleChampsLabX.add(champsX);
		this.add(coupleChampsLabX);
	}

	/**
	 * 
	 * @param position position du point.
	 * @param nom nom du point.
	 */
	private void configNom(int position, String nom) {
		JPanel coupleChampsLabNom = new JPanel();
		champsNom = new JTextField(nom);
		champsNom.setColumns(3);
		champsNom.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {
			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
			}
			@Override
			public void keyReleased(KeyEvent e) {
				Fichier.getFigureEnCours().listPoint.get(position).setNom(champsNom.getText());
				FenetrePrincipale.panneauDessin.repaint();
				FenetrePrincipale.panneauDessin.revalidate();
			}
		});

		coupleChampsLabNom.add(new JLabel("Nom"));
		coupleChampsLabNom.add(champsNom);
		this.add(coupleChampsLabNom);
	}

	/**
	 * 
	 * @param position position du point.
	 */
	private void configPosition(int position) {
		posLab.setText(position + 1 + "");
		this.add(posLab);

	}
	
	/**
	 * rafraichit les points
	 */
	public static void refresh() {
		PanneauPoint.panneauListeDePoint.repaint();
		PanneauPoint.panneauListeDePoint.revalidate();
		FenetrePrincipale.panneauPoint.repaint();
		FenetrePrincipale.panneauPoint.revalidate();
		FenetrePrincipale.panneauDessin.repaint();
		FenetrePrincipale.panneauDessin.revalidate();
	}
}