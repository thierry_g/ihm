package com.ihm.visuel;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.JFrame;
import com.ihm.config.Constantes;
import com.ihm.object.Fichier;


/**
 *Cette classe génère la Fenetre principal
 *Elle se décompose de 3 panneaux:
 *	- panneauBouton: comporte les boutons principaux (thème, type de trait, epaisseur, couleur, precedent, suivante et nouvelle figure.
 *  - panneauPoint: comporte une liste de panneauPointLigne qui eux meme comportent, la position, le nom, l'abscisse et l'ordonnée du point.
 *  - panneauDessin: zone de dessin, délimité par un repère
 *@author Team 2
 */
public class FenetrePrincipale extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//	panneauBouton: comporte les boutons principaux 
	public static PanneauBouton panneauBouton = new PanneauBouton();

	//	 panneauPoint: va comporter une liste de panneauPointLigne qui eux meme comportent, la position, le nom, l'abscisse et l'ordonnée du point.
	public static PanneauPoint panneauPoint = new PanneauPoint();

	//- panneauDessin: zone de dessin
	public static PanneauDessin panneauDessin = new PanneauDessin();

	
	/**
	 * Contructeur.
	 */
	public FenetrePrincipale() {		
		
		// génére le program principal  ayant une visibilité sur toutes les classes
		new Fichier();
		// ajout de la premiere figure
		Fichier.getProjet().ajoutFigure();
		// initialisation de la figure en cours 
		Fichier.setFigureEnCours(Fichier.getProjet().getListFigure().get(0));
		
		// config fermeture appli
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// ajout titre fenetre
		setTitle("Dessin geometrique - Team 2");
		// ajout icone
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(Constantes.PATH_LOGO));
		// config taille fenetre
		this.setBounds(0, 0, 1000, 621);
		// ajout Barre menu
		this.setJMenuBar(new BarreMenu().menuBar);
		// config layout
		this.setLayout(new BorderLayout());
		// config dimension panneauPoint
		panneauPoint.setPreferredSize(new Dimension(320,800));
		
		// ajout des différents panneaux
		this.getContentPane().add(panneauDessin, BorderLayout.CENTER);
		this.getContentPane().add(panneauPoint, BorderLayout.EAST);
		this.getContentPane().add(panneauBouton, BorderLayout.SOUTH);
		
		// rendu visible
		this.setVisible(true);
	}
}