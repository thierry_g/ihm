package basededonnees;

import static org.junit.Assert.assertTrue;
import java.io.File;
import org.junit.Before;
import org.junit.Test;
import com.ihm.basededonnees.Chargement;
import com.ihm.basededonnees.ValidationXsd;
import com.ihm.config.Constantes;
import com.ihm.object.Projet;

/*
 * La classe testChargement verifie le format d'ecriture et la creation de figure dans le projet.
 * Elle permet de charger la sauvegarde d'un projet qui est sous format XML
 * 
 * figure est une figure.
 * projet est un projet.
 * xml est le fichier de sauvegarde.
 * xsd est le fichier xsd de verification.
 * 
 * @author Thierry
 */

public class TestChargement {

	private static Projet projet;
	private static File xml;
	private static File xsd;

	/*
	 * Methode qui permet l'initialisation des parametres necessaire au test.
	 */
	
	@Before
	public void setupBeforClass() {
		
		// Creation du fichier
		xml = new File(System.getProperty("user.dir")+ File.separator + "Data" + File.separator + "testSauve.xml");
		xsd = new File((TestChargement.class.getResource(Constantes.PATH_XSD)).getPath());
	}
	
	/*
	 * Methode qui teste le format du fichier.
	 */

	@Test
	public void should_do_when_file_format() {
		assertTrue(ValidationXsd.validation(xsd, xml) == true);
	}
	
	/*
	 * Methode qui teste la creation du fichier.
	 */
	
	@Test
	public void should_do_when_projet_is_not_empty() {
		projet = Chargement.lectureFichier(xml);
		assertTrue(projet.getListFigure().size() != 0);
	}
}